import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ApiResponse, Page} from "../../pages/user/types";

const USER_KEY = 'access_token';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  private serverUrl: string = 'http://localhost:8080/api/v1/user/';
  constructor(private readonly http: HttpClient) { }

  public getUserPage(page: number = 0, size: number = 10): Observable<ApiResponse<Page>>{
    const headers = {'Authorization': `Bearer ${sessionStorage.getItem(USER_KEY)}`}
    return this.http.get<ApiResponse<Page>>(
      `${this.serverUrl}page?page=${page}&size=${size}`,
      {headers}
    )
  }
  public getUserByEmail(email: string): Observable<any>{
    const headers = {'Authorization': `Bearer ${sessionStorage.getItem(USER_KEY)}`}
    return this.http.get<any>(
      `${this.serverUrl}/email?${email}`,
      {headers}
    )
  }
}
