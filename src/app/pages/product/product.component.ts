import {Component, OnInit} from '@angular/core';
import {AsyncPipe, CurrencyPipe, NgClass} from "@angular/common";
import {NavComponent} from "../../_shared/nav/nav.component";
import {FormBuilder} from "@angular/forms";
import { Router} from "@angular/router";
import {ProductService} from "../../services/product/product.service";
import {HttpClientModule} from "@angular/common/http";
import {Product} from "./types";
@Component({
  selector: 'app-product',
  standalone: true,
  imports: [
    CurrencyPipe,
    NavComponent,
    AsyncPipe,
    HttpClientModule,
    NgClass,
  ],
  templateUrl: './product.component.html',
  styleUrl: './product.component.css'
})
export class ProductComponent implements OnInit {
  protected products: Product[]
  protected currentPage: number
  protected totalPages: number
  protected totalElements: number
  protected firstPage: boolean
  protected lastPage: boolean
  protected pageState: string
  constructor(
    private formBuilder: FormBuilder,
    private productService: ProductService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.getProductPage();
  }
  getProductPage(): void {
    this.productService.getProductPage().subscribe({
      next: response => {
        this.products = response.data.content
        this.currentPage = response.data.number
        this.totalPages = response.data.totalPages
        this.totalElements = response.data.totalElements
        this.firstPage = response.data.last
        this.lastPage = response.data.last
        this.pageState = "APP_LOADED"
      },
      error: err => {
        this.products = []
        this.currentPage = 0
        this.totalPages = 0
        this.totalElements = 0
        this.firstPage = true
        this.lastPage = true
        this.pageState = "APP_ERROR"
      }
    })
  }
}
