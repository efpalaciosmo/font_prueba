import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {ApiResponse, Page} from "../../pages/product/types";

const USER_KEY = 'access_token';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private serverUrl: string = 'http://localhost:8080/api/v1/';
  constructor(private readonly http: HttpClient) { }

  public getProductPage(page: number = 0, size: number = 10): Observable<ApiResponse<Page>>{
    const headers = {'Authorization': `Bearer ${sessionStorage.getItem(USER_KEY)}`}
    return this.http.get<ApiResponse<Page>>(
      `${this.serverUrl}product/page?page=${page}&size=${size}`,
      {headers}
    )
  }
  public getProductById(idProduct: number): Observable<any>{
    const headers = {'Authorization': `Bearer ${sessionStorage.getItem(USER_KEY)}`}
    return this.http.get<any>(
      `${this.serverUrl}/products/page?pa`,
      {headers}
    )
  }
}
