import {Component, OnInit} from '@angular/core';
import {User} from "./types";
import {UserService} from "../../services/user/user.service";
import {RegisterComponent} from "../modal/register/register.component";

@Component({
  selector: 'app-user',
  standalone: true,
  imports: [
    RegisterComponent
  ],
  templateUrl: './user.component.html',
  styleUrl: './user.component.css'
})
export class UserComponent implements OnInit {
  protected users: User[]
  protected currentPage: number
  protected totalPages: number
  protected totalElements: number
  protected firstPage: boolean
  protected lastPage: boolean
  protected pageState: string

  constructor(private readonly userService: UserService) {
  }

  ngOnInit(): void {
    this.getUserPage();
  }
  getUserPage(): void {
    this.userService.getUserPage().subscribe({
      next: response => {
        this.users = response.data.content
        this.currentPage = response.data.number
        this.totalPages = response.data.totalPages
        this.totalElements = response.data.totalElements
        this.firstPage = response.data.last
        this.lastPage = response.data.last
        this.pageState = "APP_LOADED"
      },
      error: err => {
        this.users = []
        this.currentPage = 0
        this.totalPages = 0
        this.totalElements = 0
        this.firstPage = true
        this.lastPage = true
        this.pageState = "APP_ERROR"
      }
    })
  }
}
