import {Component, OnInit} from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {initFlowbite} from "flowbite";
import {LoginComponent} from "./pages/login/login.component";
import {StorageService} from "./services/storage/storage.service";
import {HttpClientModule} from "@angular/common/http";
import {ProductComponent} from "./pages/product/product.component";
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, LoginComponent, HttpClientModule, ProductComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements OnInit {
  constructor(private readonly storageService:StorageService) {
  }
  ngOnInit(): void {
    initFlowbite();
  }
}
