export interface ApiResponse<T> {
  message: string,
  data: T
}
export interface Page {
  content: Product[],
  pageable: {
    sort: {
      sorted: boolean,
      unsorted: boolean,
      empty: boolean
    },
    pageNumber: number,
    pageSize: number,
    offset: number,
    paged: boolean,
    unpaged: boolean
  },
  last: boolean,
  totalPages: number,
  totalElements: number,
  first: boolean,
  number: number,
  sort: {
    sorted: boolean,
    unsorted: boolean,
    empty: boolean
  },
  numberOfElements: number,
  size: number,
  empty: boolean
}
export interface Product {
  name: string;
  model: string;
  description: string;
  price: number;
  quantity: number;
}
