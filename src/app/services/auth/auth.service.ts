import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private readonly http: HttpClient) { }
  login(email: string, password: string): Observable<any>{
    return this.http.post(
      "http://localhost:8080/api/v1/auth/login", {
        email,
        password
      },
      httpOptions
    )
  }

  register(request: RegisterRequest): Observable<any>{
    return this.http.post(
      "http://localhost:3000",
      request,
      httpOptions
    )
  }
}

interface RegisterRequest {
  "firstName": string,
  "lastName": string,
  "email": string,
  "password": string,
  "role": string
}
