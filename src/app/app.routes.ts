import {Routes} from '@angular/router';
export const routes: Routes = [
  {
    path: 'login',
    pathMatch: 'full',
    loadComponent: () => import('./pages/login/login.component')
      .then(mod => mod.LoginComponent),
  },
  {
    path: 'register',
    pathMatch: 'full',
    loadComponent: () => import('./pages/register/register.component')
      .then(mod => mod.RegisterComponent),
  },
  {
    path: 'product',
    pathMatch: 'full',
    loadComponent: () => import('./pages/product/product.component')
      .then(mod => mod.ProductComponent),
  },
  {
    path: 'user',
    pathMatch: 'full',
    loadComponent: () => import('./pages/user/user.component')
      .then(mod => mod.UserComponent),
  },
  {
    path: '',
    pathMatch: 'full',
    loadComponent: () => import('./pages/login/login.component')
      .then(mod => mod.LoginComponent),
  }
]
