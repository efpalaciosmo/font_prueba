# Front para pruba Equitel

## Instalación
Para instalar el proyecto seguir los siguientes pasos
```shell
git clone https://gitlab.com/efpalaciosmo/font_prueba front
cd front
npm install
ng serve
```

En el url `http://localhost:4200/` se encuentra la aplicación, a la que puede iniciar con los datos
- correo: admin@mail.com
- password: cKCGuqDGhXBM

![Login](./assets/login.png "Login")
![Register](./assets/registrar_usuario.png "Register")
![Products](./assets/products.png "Products")
![Usuarios](./assets/usuario.png "Usuarios")
