import { Injectable } from '@angular/core';

const USER_KEY = 'access_token';
@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }
  clean(): void {
    window.sessionStorage.clear()
  }

  public saveUser(token: any): void{
    window.sessionStorage.removeItem(USER_KEY)
    window.sessionStorage.setItem(USER_KEY, token.token)
  }

  public getUser(): any {
    const user = window.sessionStorage.getItem(USER_KEY)
    if(user){
      return user
    }
    return null
  }

  public isLoggedIn(): boolean {
    const user = window.sessionStorage.getItem(USER_KEY)
    return !!user;
  }
}
