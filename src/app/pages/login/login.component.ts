import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../../services/auth/auth.service";
import {StorageService} from "../../services/storage/storage.service";
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators
} from "@angular/forms";
import {NgClass} from "@angular/common";

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    FormsModule,
    ReactiveFormsModule,
    NgClass
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit {
  form: FormGroup = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  })
  submitted: boolean = false;
  isLoggedIn: boolean = false
  isLoginFailed: boolean = false
  errorMessage: string = ''
  role: string = ''
  constructor(
    private readonly router: Router,
    private readonly formBuilder: FormBuilder,
    private readonly authService: AuthService,
    private readonly storageService: StorageService) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    })
    if(this.storageService.isLoggedIn()){
      this.role = this.storageService.getUser().role
    }
  }
  onSubmit(): void {
    this.submitted = true;
    if(this.form.invalid){
      console.log(`Invalid form ${this.form.invalid}`)
      return;
    }
    this.authService.login(this.form.value.email!, this.form.value.password!).subscribe({
      next: response => {
        console.log(response)
        this.storageService.saveUser(response.data);
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.role = this.storageService.getUser().role
        this.router.navigateByUrl('product')
      },
      error: err => {
        console.log(err)
        this.errorMessage = err.error.message
        this.isLoginFailed = true
      }
    })
  }
  reloadPage(): void {
    window.location.reload()
  }
  get f(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }

  onReset(): void {
    this.submitted = false;
    this.form.reset();
  }
}
interface LoginRequest {
  email: string,
  password: string
}
